#!/bin/bash

# lisa V816.0.3.0.UKOMIXM
VENDOR_PATH="../"

# Cleanup leftlowers
rm -rf MiuiCamera*
for i in $(find . | grep Zone); do
    rm -rf $i
done

# system
#
# Updated manually from LineageOS $OUT:
# system/lib64/libgui_shim_miuicamera.so | 755
#
cp "$VENDOR_PATH"/vendor/proprietary/system/lib64/libcamera_algoup_jni.xiaomi.so system/lib64/libcamera_algoup_jni.xiaomi.so
cp "$VENDOR_PATH"/vendor/proprietary/system/lib64/libcamera_mianode_jni.xiaomi.so system/lib64/libcamera_mianode_jni.xiaomi.so
cp "$VENDOR_PATH"/vendor/proprietary/system/lib64/libmicampostproc_client.so system/lib64/libmicampostproc_client.so
cp "$VENDOR_PATH"/vendor/proprietary/system/lib64/vendor.xiaomi.hardware.campostproc@1.0.so system/lib64/vendor.xiaomi.hardware.campostproc@1.0.so
cp "$VENDOR_PATH"/vendor/proprietary/system/priv-app/MiuiCamera/MiuiCamera.apk system/priv-app/MiuiCamera/MiuiCamera.apk
zipalign -p 4 system/priv-app/MiuiCamera/MiuiCamera.apk system/priv-app/MiuiCamera/MiuiCamera_aligned.apk
apksigner sign --key ~/los21/build/target/product/security/testkey.pk8 --cert ~/los21/build/target/product/security/testkey.x509.pem system/priv-app/MiuiCamera/MiuiCamera_aligned.apk
sleep 2
mv system/priv-app/MiuiCamera/MiuiCamera_aligned.apk system/priv-app/MiuiCamera/MiuiCamera.apk
rm -rf system/priv-app/MiuiCamera/MiuiCamera*.idsig

# vendor
cp "$VENDOR_PATH"/vendor/proprietary/vendor/etc/camera/st_license.lic system/vendor/etc/camera/camera_cnf.txt
cp "$VENDOR_PATH"/vendor/proprietary/vendor/etc/camera/st_license.lic system/vendor/etc/camera/st_license.lic

zip -r MiuiCamera_lisa-v$(grep version module.prop | grep -Eo '[0-9].{1,4}' | head -1).zip * -x build.sh
